package com.techtalk.elasticsearch.controller;

import com.techtalk.elasticsearch.model.Book;
import com.techtalk.elasticsearch.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/books/searchBooksByTitleWithMatchQuery")
    public List<Book> searchBooksByTitleWithMatchQuery(@RequestParam String title) throws IOException {
        return bookService.searchBooksByMatchQuery("title", title);
    }

    @GetMapping("/books/searchBookByMatchPhraseQuery")
    public List<Book> searchBookByMatchPhraseQuery(@RequestParam String title) throws IOException {
        return bookService.searchBookByMatchPhraseQuery("title", title);
    }

    @GetMapping("/books/searchBookByFuzzyMatchQuery")
    public List<Book> searchBookByFuzzyMatchQuery(@RequestParam String title) throws IOException {
        return bookService.searchBookByFuzzyMatchQuery("title", title);
    }
}
