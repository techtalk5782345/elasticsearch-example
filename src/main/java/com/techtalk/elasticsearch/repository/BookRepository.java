package com.techtalk.elasticsearch.repository;

import com.techtalk.elasticsearch.model.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends ElasticsearchRepository<Book, String> {
    // Additional custom queries can be added here if needed
}