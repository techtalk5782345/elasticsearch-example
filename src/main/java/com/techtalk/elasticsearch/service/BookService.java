package com.techtalk.elasticsearch.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.elasticsearch.core.search.Hit;
import com.techtalk.elasticsearch.model.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class BookService {

    @Autowired
    private ElasticsearchClient elasticsearchClient;

    private static final String BOOK_INDEX = "books";

    public void saveBook(List<Book> bookList) throws IOException {

        BulkRequest.Builder br = new BulkRequest.Builder();

        for (Book book : bookList) {
            br.operations(op -> op
                    .create(idx -> idx
                            .index(BOOK_INDEX)
                            .id(book.getId())
                            .document(book)
                    )
            );
        }

        BulkResponse result = elasticsearchClient.bulk(br.build());

        if (result.errors()) {
            log.error("Bulk had errors");
            for (BulkResponseItem item : result.items()) {
                if (item.error() != null) {
                    log.error(item.error().reason());
                }
            }
        }
    }


    public List<Book> searchBooksByMatchQuery(String field, String queryText) throws IOException {


        SearchResponse<Book> response = elasticsearchClient.search(searchRequestBuilder -> searchRequestBuilder
                        .index(BOOK_INDEX)
                        .query(q -> q
                                .match(t -> t
                                        .field(field)
                                        .query(queryText)
                                )
                        ),
                Book.class
        );

        return convertHitsToBooks(response.hits().hits());
    }

    public List<Book> searchBookByMatchPhraseQuery(String field, String queryText) throws IOException {
        SearchResponse<Book> response = elasticsearchClient.search(searchRequestBuilder -> searchRequestBuilder
                        .index(BOOK_INDEX)
                        .query(q -> q
                                .matchPhrase(t -> t
                                        .field(field)
                                        .query(queryText)
                                )
                        ),
                Book.class
        );

        return convertHitsToBooks(response.hits().hits());
    }

    public List<Book> searchBookByFuzzyMatchQuery(String field, String queryText) throws IOException {
        SearchResponse<Book> response = elasticsearchClient.search(searchRequestBuilder -> searchRequestBuilder
                        .index(BOOK_INDEX)
                        .query(q -> q
                                .fuzzy(t -> t
                                        .field(field)
                                        .fuzziness("AUTO")
                                        .value(queryText)
                                )
                        ),
                Book.class
        );

        return convertHitsToBooks(response.hits().hits());
    }

    private List<Book> convertHitsToBooks(List<Hit<Book>> hitBookList) {
        List<Book> books = new ArrayList<>();
        for (Hit<Book> hit : hitBookList) {
            books.add(hit.source());
        }
        return books;
    }
}
