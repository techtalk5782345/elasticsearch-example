package com.techtalk.elasticsearch;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import co.elastic.clients.elasticsearch.core.CountRequest;
import co.elastic.clients.elasticsearch.core.CountResponse;
import co.elastic.clients.elasticsearch.indices.*;
import com.techtalk.elasticsearch.model.Book;
import com.techtalk.elasticsearch.service.BookService;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@SpringBootApplication
//@EnableSwagger2
public class ElasticsearchApplication {

    @Autowired
    private BookService bookService;

    @Autowired
    private ElasticsearchClient elasticsearchClient;

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchApplication.class, args);
    }

//    @Bean
//    public Docket productApi() {
//        return new Docket(DocumentationType.SWAGGER_2).select()
//                .apis(RequestHandlerSelectors.basePackage("com.techtalk.elasticsearch")).build();
//    }

    @PostConstruct
    public void executeAfterStartup() throws IOException {
        createIndex("books");

        CountRequest countRequest = new CountRequest.Builder()
                .index("books")
                .build();

        CountResponse countResponse = elasticsearchClient.count(countRequest);
        long documentCount = countResponse.count();

        if (documentCount == 0) {
            Book book1 = new Book();
            book1.setId(UUID.randomUUID().toString());
            book1.setAuthor("Shashi Tharoor");
            book1.setTitle("Ambedkar: A Life");

            Book book2 = new Book();
            book2.setId(UUID.randomUUID().toString());
            book2.setAuthor("Shubhra Gupta");
            book2.setTitle("Irrfan Khan: A Life in Movies");

            List<Book> bookList = Arrays.asList(book1, book2);
            bookService.saveBook(bookList);
        } else {
            System.out.println("No records added");
        }
    }

    public boolean createIndex(String indexName) throws IOException {

        GetIndexRequest getIndexRequest = GetIndexRequest.of(builder -> builder.index(indexName));

        try {
            GetIndexResponse getIndexResponse = elasticsearchClient.indices().get(getIndexRequest);
            Map<String, IndexState> result = getIndexResponse.result();

            if (!result.isEmpty()) {
                System.out.println("Index already exists: " + indexName);
                return true;  // No need to create the index if it already exists
            }
        } catch (ElasticsearchException e) {
            // Handle the case where the index does not exist
            if (e.status() == 404) {
                System.out.println("Index not found: " + indexName);
            } else {
                // Handle other Elasticsearch exceptions
                throw e;
            }
        }

        // Create the index since it doesn't exist
        CreateIndexRequest createIndexRequest = new CreateIndexRequest.Builder()
                .index(indexName)
                .build();

        CreateIndexResponse createIndexResponse = elasticsearchClient.indices().create(createIndexRequest);

        if (createIndexResponse.acknowledged()) {
            System.out.println("Index created successfully: " + indexName);
        } else {
            System.out.println("Failed to create index: " + indexName);
        }
        return false;
    }
}
