# Hello World elasticsearch Example

This is a simple hello world elasticsearch example using springboot.

## Overview

This project demonstrates the overview of elasticsearch.

## Prerequisites

Make sure you have the following installed:

- Java (version 8 or higher)
- [Elasticsearch](https://www.elastic.co/downloads/elasticsearch)
- [Kibana](https://www.elastic.co/downloads/kibana)

## Getting Started

1. Clone the repository:

    ```
    git clone https://gitlab.com/techtalk5782345/elasticsearch-example.git
    ```

2. Navigate to the project directory:

    ```
    cd elasticsearch
    ```
3. Run the Spring Boot Application

   ``` 
   ./mvnw spring-boot:run
   ```

4. Once your Spring Boot Application is up. Open the below swagger link in order to test the elasticsearch features

   ```
   http://localhost:8080/swagger-ui/index.html#/
   ```
